# xPertre

Program pohon keputusan sederhana dengan aturan berbasis XML.
Aturan yang telah dibuat dapat digunakan di berbagai bahasa pemrograman.

Alasan digunakannya XML adalah karena mudah dibuat, mudah dibaca oleh manusia dan mesin, dan dapat dipakai untuk berbagai keperluan.
Mungkin ada yang bertanya kenapa tidak pakai JSON? Karena dari segi dukungan, bahasa pemrograman yang mendukung XML secara *native* lebih banyak ketimbang JSON. Walaupun memang XML lebih `boros` daripada JSON karena merupakan bahasa markah.

## Format Aturan

Aturan yang didefinisikan semuanya harus berada dalam tag `ruleset`, seperti pada contoh di bawah ini.

```xml
<ruleset>
    <rule id="Rule1">
        ...
    </rule>
    <rule>
        ...
    </rule>
    <rule>
        ...
            <link href="#Rule1"/>
        ...
    </rule>
    ...
</ruleset>
```

Aturan juga dapat ditautkan atau diarahkan ke aturan lain menggunakan tag `link` dengan atribut `href` yang menunjukan lokasi aturan. Dalam contoh di atas, atribut `href` bernilai `#Rule1` yang berarti tautan ini diarahkan ke aturan dengan id `Rule1`.

Secara asali (*default*) program akan menggunakan aturan pertama (tag `rule`) yang didefinisikan kecuali terdapat atribut `first` pada tag `ruleset`.

```xml
<ruleset first="HeartDisease">
    <metadata>
        <description>Multiple Disease Diagnosis</description>
        <author>Medicae .Inc</author>
    </metadata>
    <rule id="SkinDisease">
        <condition>...</condition>
        ...
    </rule>
    ...
    <rule id="HeartDisease">
        ...
    </rule>
</ruleset>
```

Jadi, walaupun aturan `SkinDisease` didefinisikan paling pertama, tapi karena `first` bernilai `HeartDisease` maka aturan `HeartDisease`-lah yang pertama kali dijalankan.

Saat ini aturan hanya mendukung logika sederhana `if-then-else`, untuk selanjutnya diharapakan dapat mendukung logika matematika dan perbandingan. Di dalam aturan, dibutuhkan setidanya satu kondisi dan satu aksi.

```xml
<rule>
    <condition>Would you prefer automatic car?</condition>
    <do>
        <rule id="Color">
            <condition>Do you like silver car?</condition>
            <do>
                <set>...</set>
            </do>
        </rule>
    <do>
    <else>
        <set>Then, ...</set>
    </else>
</rule>
```

Aturan dalam tag `else` akan dijalankan jika kondisi salah atau tidak terpenuhi. tag `set` digunakan sebagai nilai akhir (*end node*) dan jika terdapat teks di dalamnya, maka akan ditampilkan ke perangkat keluaran. Aturan juga dapat bercabang, yaitu dengan mendefinisikan tag `rule` di dalam tag `do` atau `else`.

## Todo

 - [x] Aturan berbasis XML
 - [X] Implementasi dalam Python dan Java
 - [ ] Aturan logika `and`, `or`, `xor`, dan `not`
 - [ ] Perbandingan aritmetika `<`, `>`, `<=`, `>=`, `==`, dan `!=`